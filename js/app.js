$(function(){
    $('#language-selector-form input[type=submit]').css('display', 'none');
    $('#preferred_lang' ).change(function() {
        $('#language-selector-form form').submit();
    });
});